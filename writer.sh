#!/bin/bash

MOUNTDIR="/media/stulluk/f504259d-96fb-4f7c-a06e-334d75848731/WAFTEST"
GARBAGEDIR="$MOUNTDIR/garbage"
RESULTFILE="results.txt"

MAXBLOCKSIZE=$((16*1024*1024))

rm -rf $GARBAGEDIR

mkdir -p $GARBAGEDIR

echo "" > $RESULTFILE

numfiles=1000

for (( bs=8 ; bs<=$MAXBLOCKSIZE ; bs*=2 ))
do
	start_sector=$(sudo smartctl -d sat -a /dev/sde | grep "241 T" | awk '{print $10}')
	printf "\nCurrent start_sector is $start_sector \n" | tee -a $RESULTFILE
	
	for filenum in $(seq 1 1 ${numfiles})
	do
		printf "\nfilenum is $filenum bs is $bs \n"
		sudo dd if=/dev/urandom of=$GARBAGEDIR/file.$filenum bs=$bs count=1 conv=fdatasync
		sync
	done
	sync

	for (( a=1 ; a<2 ; a++ ))
	do	
		end_sector=$(sudo smartctl -d sat -a /dev/sde | grep "241 T" | awk '{print $10}')
		printf "\nCurrent end_sector is $end_sector \n" | tee -a $RESULTFILE
		sleep 10
	done
	
	end_sector=$(sudo smartctl -d sat -a /dev/sde | grep "241 T" | awk '{print $10}')
	diff=$((end_sector-start_sector))
	TBW=$((diff*512))
	WAF=$(echo "scale=3; $TBW/$numfiles/$bs" | bc )
	printf "\n BS:$bs Numfiles:$numfiles Start:$start_sector End:$end_sector Diff:$diff  TBW:$TBW  WAF=$WAF" | tee -a $RESULTFILE
	
done

rm -rf $GARBAGEDIR
